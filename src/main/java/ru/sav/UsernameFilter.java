package ru.sav;

import javax.servlet.*;
import java.io.IOException;

public class UsernameFilter implements Filter {
    public static final String PARAM_NAME = "UserToBan";
    private String userToBan;


    public void init(FilterConfig filterConfig) throws ServletException {
        userToBan = filterConfig.getInitParameter(PARAM_NAME);
        System.out.println("Username filter init");
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("UserFilter");
        String name = servletRequest.getParameter("userName");
        if (userToBan.equalsIgnoreCase(name)) {
            servletResponse.getWriter().write("�� �� �������");
            System.out.println("Filtered");
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    public void destroy() {

    }
}
