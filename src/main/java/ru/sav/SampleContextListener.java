package ru.sav;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class SampleContextListener implements ServletContextListener {
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        System.out.println(">>>>Initialized");
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println(">>>>Destroyed");
    }
}
